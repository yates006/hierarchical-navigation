﻿using Xamarin.Forms;
using System;

namespace HierarchicalNavigation
{
    public class PageInfo
    {
        public int Number;
        public string Content;
    }

    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            var info = new PageInfo
            {
                Number = 1,
                Content = "This is the first page!",

            };

            var page = new HierarchicalNavigationPage();
            page.BindingContext = info;

            MainPage = new NavigationPage(page);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
