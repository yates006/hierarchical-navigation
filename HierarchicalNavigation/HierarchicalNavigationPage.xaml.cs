﻿using Xamarin.Forms;
using System;

namespace HierarchicalNavigation
{

    public partial class HierarchicalNavigationPage : ContentPage
    {
        public class PageData
        {
            public string TextContent { get; set; }
        }

        public HierarchicalNavigationPage()
        {
            InitializeComponent();
        }

        async void OnSecondPageButtonClicked(object sender, EventArgs e)
        {

            var pageData = new PageData
            {
                TextContent = TextEntry.Text
            };

            var secondPage = new SecondPage();
            secondPage.BindingContext = pageData;

            var answer = await DisplayAlert("Leaving so soon?", "Would you really like to leave this page?", "Yes", "No");

            if (answer)
            {
                await Navigation.PushAsync(secondPage);
            }
        }
    }
}
